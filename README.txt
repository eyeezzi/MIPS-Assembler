Project 1 for CS 2506: Computer Organization II

See pdf document for spec

To compile: gcc -lm -g -Wall assembler.c -o assembler

To run: ./assembler <input file> <output file>